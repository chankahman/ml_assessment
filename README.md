# ML_Assessment

This REST project is to solve technical assessment base on following user story and requirements.

User story:
As Product Manager, I would like to manage users’ accesses to new features via feature switches, i.e. enabling/disabling certain feature based on a user’s email and feature names).

Requirements:
• GET /feature?email=XXX&featureName=XXX
This endpoint receives   (user’s email) and featureName as request parameters and returns the following response in JSON format.

• POST /feature
This endpoint receives the following request in JSON format and returns an empty response with HTTP Status OK (200) when the database is updated successfully, otherwise returns Status Not Modified (304).

## Extra Features
• OAuth
• SoftDeletable
• Logging
• Form

## Requirements
Project framework: Symfony(PHP) - https://symfony.com/
Requirements: 
• Linux server
• PHP7.1+
• MySQL MariaDB (Create a database, prepare database host, port, name, user, password data)
• Git
• Composer

## Installation:
```bash
git clone https://gitlab.com/chankahman/ml_assessment
cd ml_assessment
mv .env.example .env (Update DATABASE_* settings)
composer install
php bin/console make:migration
php bin/console doctrine:migrations:migrate
php bin/console fos:oauth-server:create-client  --grant-type=password (Copy Client ID & Secret) 
php bin/console SampleDataCommand (Generate sample data)
point your site directory to /public folder, or run symfony serve to start local dev server
```

## Documentations:

- The project consists of two roles(ROLE_USER & ROLE_ADMIN)
- Project manager uses ROLE_ADMIN, other users use ROLE_USER
- The two get/set user feature API, are only granted to ROLE_ADMIN only, else return 403 code.

- Here I use Postman(https://www.postman.com/) as HTTP testing tool.
API Document: https://documenter.getpostman.com/view/1015061/TW6wH8Nu

- You can download Postman collection and environment from the link below and import to ease yourself.
https://drive.google.com/drive/folders/1X3QznCWAOhVlTveTmpUezldnAng37Muy?usp=sharing

- You are free to use the host below to test too, it's my own testing cloud server(DigitalOcean).
Host: http://167.71.208.194:8282
Client ID: 1_5ylkirxfsts8sok8c44osc8o04w4skkgc8gwkcs0c00ks44wos
Client Secret: 2vqdanvnqbcwg8wc0cg404s00cs40g000s80s8cck4g4so40oo
Username: pm001@mail.com
Password: abc@123

## How to test:
1. Call OAuth Sign In API(Pass valid client_id, client_secret, username(email) & password), got "access_token" from response.
2. Call Get/Set User Feature(Pass Authorization header with value "Bearer <access_token>" and necessary request parameters or data).

## How to test with Postman:
- With postman environment feature, we can automate many steps.
1. Import the Postman collection and environment above.
2. Call OAuth Sign In API(Pass valid client_id, client_secret, username(email) & password), got "access_token" from response.
    - If success, Tests feature will auto set {{BEARER}} value. 
3. Call Get/Set User Feature(Pass Authorization header with value "Bearer <access_token>" and necessary request parameters or data).
    - Authorization feature, will auto add Authorization header with {{BEARER}} value.