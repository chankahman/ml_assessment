<?php

return [
    'user' => [
        'email' => [
            'not_found' => '用户邮箱地址(%email%)不存在。'
        ]
    ],
    'feature' => [
        'name' => [
            'not_blank' => '功能名称不能是空白的。',
            'not_found' => '功能名称(%name%)不存在。',
            'closed' =>'功能状态目前为关闭。'
        ]
    ],
    'enable' => [
        'invalid' => '错误开通选项(%option%)。'
    ]
];