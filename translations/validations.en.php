<?php

return [
    'user' => [
        'email' => [
            'not_found' => 'User email(%email%) not found.'
        ]
    ],
    'feature' => [
        'name' => [
            'not_blank' => 'Feature name cannot be blank.',
            'not_found' => 'Feature name(%name%) not found.',
            'closed' =>'Feature is closed.'
        ]
    ],
    'enable' => [
        'invalid' => 'Invalid enable option(%option%).'
    ]
];