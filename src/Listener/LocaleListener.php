<?php

namespace App\Listener;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LocaleListener implements EventSubscriberInterface
{
    protected $container;
    protected $request;
    protected $session;
    protected $defaultLocale;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->defaultLocale = $container->getParameter('default_locale');
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();
        
        //Set locale if header contains 
        if ($request->headers->has('Locale')) {
            $locale = $request->headers->get('Locale');
        } else {
            $locale = $request->getSession()->get('_locale', $this->defaultLocale);
        }
        $request->getSession()->set('_locale', $locale);
        $request->setLocale($locale);
    }

    public static function getSubscribedEvents()
    {
        return [
            RequestEvent::class => [['onKernelRequest', 17]],
        ];
    }

}
