<?php

namespace App\Form\User;

use App\Traits\DbTrait;
use App\Entity\User\User;
use App\Entity\Feature\Feature;
use App\Entity\User\UserFeature;
use App\Form\User\TypeUserFeature;
use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class SetUserFeatureType extends AbstractType
{
    use DbTrait;
    
    protected $translator;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->entityManager = $options['entity_manager'];
        $this->translator = $options['translator'];

        $this->Db_init();

        $builder
            ->add('email', EmailType::class, [
                'constraints' => [ new NotBlank() ],
            ])
            ->add('featureName', TextType::class, [
                'constraints' => [ new NotBlank() ],
            ])
            ->add('enable', ChoiceType::class, [
                'constraints' => [ new NotBlank() ],
                'choices' => [ 'yes', 'no' ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('entity_manager');
        $resolver->setRequired('translator');

        $resolver->setDefaults([
            'data_class' => TypeUserFeature::class,
            'csrf_protection' => false,
            'constraints' => [
                new Callback([
                    'callback' => [$this, 'validate']
                ])
            ]
        ]);
    }

    public function validate($typeUserFeature, ExecutionContextInterface $context)
    {
        $errors = [];

        //Validate enable value
        $typeUserFeature->setEnable($typeUserFeature->getEnable() === 'yes');

        list($user, $feature, $userFeature) = $this->Db_useRepository(UserFeature::class)->findByEmailAndFeatureName($typeUserFeature->getEmail(), $typeUserFeature->getFeatureName());

        //Validate user
        if(!$user) { $errors['email'] = ['user.email.not_found', ['%email%' => $typeUserFeature->getEmail()], 'validations']; }
        //Validate feature
        if(!$feature) { $errors['featureName'] = ['feature.name.not_found', ['%name%' => $typeUserFeature->getFeatureName()], 'validations']; }
        else if(!$feature->getOpened()) { $errors['featureName'] = ['feature.name.closed', ['%name%' => $typeUserFeature->getFeatureName()], 'validations']; }

        if($errors) {
            foreach($errors as $property => list($error, $errorParams, $errorDomain)) {
                if(!$error) { continue; }
                $message = $this->translator->trans($error, $errorParams, $errorDomain);
                $context->buildViolation($message)->atPath($property)->addViolation();
            }
            return;
        }

        if(!$userFeature) {
            $userFeature = new UserFeature();
            $userFeature->setUser($user);
            $userFeature->setFeature($feature);
        }
        $typeUserFeature->setUserFeature($userFeature);
    }
}