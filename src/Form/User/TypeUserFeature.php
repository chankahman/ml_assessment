<?php

namespace App\Form\User;

use App\Entity\User\UserFeature;

class TypeUserFeature
{
    private $email;
	private $featureName;
	private $enable;
    private $userFeature;

    public function getEmail(){
		return $this->email;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function getFeatureName(){
		return $this->featureName;
	}

	public function setFeatureName($featureName){
		$this->featureName = $featureName;
	}

	public function getEnable(){
		return $this->enable;
	}

	public function setEnable($enable){
		$this->enable = $enable;
	}
    
    public function getUserFeature(){
		return $this->userFeature;
	}

	public function setUserFeature(UserFeature $userFeature){
		$this->userFeature = $userFeature;
	}
}