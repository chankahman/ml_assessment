<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait Byable
{
    /**
     * @ORM\Column(type="integer")
     * @var int
     * */
    private $created_by = 0;

    /**
     * @ORM\Column(type="integer")
     * @var int
     * */
    private $updated_by = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int
     * */
    private $deleted_by = null;

    public function getCreatedBy(): ?int
    {
        return $this->created_by;
    }

    public function setCreatedBy(int $created_by): self
    {
        $this->created_by = $created_by;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updated_by;
    }

    public function setUpdatedBy(int $updated_by): self
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    public function getDeletedBy()
    {
        return $this->deleted_by;
    }

    public function setDeletedBy(int $deleted_by)
    {
        $this->deleted_by = $deleted_by;

        return $this;
    }
}
