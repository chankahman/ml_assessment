<?php

namespace App\Entity\User;

use App\Entity\UserPackage\UserPackage;

trait UserProperties
{
    private $password_plain;

    public function getPasswordPlain(){
		return $this->password_plain;
	}

	public function setPasswordPlain($password_plain){
		$this->password_plain = $password_plain;
	}
}