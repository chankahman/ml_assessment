<?php

namespace App\Repository\User;

use App\Constants\RoleConstants;
use App\Traits\DbTrait;
use App\Entity\User\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    use DbTrait;

    protected $passwordEncoder;

    public function __construct(
        ManagerRegistry $registry, 
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        
        parent::__construct($registry, User::class);

        $this->Db_init();
    }

    //Using a Custom Query to Load the User
    //https://symfony.com/doc/current/security/user_provider.html
    public function loadUserByUsername(string $usernameOrEmail)
    {
        return $this->entityManager->createQuery(
            'SELECT u
            FROM App\Entity\User\User u
            WHERE u.username = :query
                OR u.email = :query'
        )
        ->setParameter('query', $usernameOrEmail)
        ->getOneOrNullResult();
    }

    public function signupUser(User $user, $roles = [RoleConstants::ROLE_USER])
    {
        $user->setRoles($roles);
        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPasswordPlain()));
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

}
