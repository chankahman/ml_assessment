<?php

namespace App\Repository\User;

use App\Traits\DbTrait;
use App\Entity\User\User;
use App\Entity\Feature\Feature;
use App\Entity\User\UserFeature;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method UserFeature|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserFeature|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserFeature[]    findAll()
 * @method UserFeature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserFeatureRepository extends ServiceEntityRepository
{
    use DbTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserFeature::class);

        $this->Db_init();
    }

    public function findByEmailAndFeatureName($email, $featureName)
    {
        $user = $this->Db_useRepository(User::class)->findOneBy(['email' => $email, 'deletedAt' => NULL]);
        $feature = $this->Db_useRepository(Feature::class)->findOneBy(['name' => $featureName, 'deletedAt' => NULL]);
        $userFeature = null;
        if($user && $feature) {
            $userFeature = $this->Db_useRepository(UserFeature::class)->findOneBy([
                'user' => $user,
                'feature' => $feature,
                'deletedAt' => NULL
            ]);
        }
        return [$user, $feature, $userFeature];
    }

}
