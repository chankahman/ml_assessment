<?php 
namespace App\Command;

use App\Traits\DbTrait;
use App\Entity\User\User;
use App\Entity\Feature\Feature;
use App\Constants\RoleConstants;
use App\Entity\User\UserFeature;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SampleDataCommand extends Command
{
    protected static $defaultName = 'SampleDataCommand';

    use DbTrait;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure() {}

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //Clear database
        /* $tableName = $_SERVER['DATABASE_NAME'];
        $tablesSql = "SELECT table_name AS 'table_name' FROM information_schema.tables WHERE table_schema = '$tableName'";
        $tables = $this->Db_select($tablesSql); */
        $tables = [
            'user_features',
            'features',
            'users'
        ];
        $this->Db_update("SET foreign_key_checks = 0");
        foreach($tables as $table) {
            //$truncateTable = $table['table_name'];
            $truncateTable = $table;
            $this->Db_update("TRUNCATE TABLE $truncateTable");
        }
        $this->Db_update("SET foreign_key_checks = 1");

        $this->Db_beginTransaction();

        /* Begin insert Users */
        $defaultPassword = 'abc@123';
        $usersData = [
            ['pm001@mail.com', [RoleConstants::ROLE_ADMIN]], //Project Manager
            ['usr001@mail.com', [RoleConstants::ROLE_USER]], //Demo User 1
            ['usr002@mail.com', [RoleConstants::ROLE_USER]], //Demo User 2
        ];
        $users = [];
        foreach($usersData as $uData) {
            $user = new User();
            $user->setEmail($uData[0]);
            $user->setPasswordPlain($defaultPassword);
            $this->Db_useRepository(User::class)->signupUser($user, $uData[1]);
            $users[] = $user;
        }
        /* End insert Users */

        /* Begin insert Features */
        $featuresData = [
            ['feature_001', 'Feature 001', true],
            ['feature_002', 'Feature 002', true],
            ['feature_003', 'Feature 003', false],
        ];
        $features = [];
        foreach($featuresData as $fData) {
            $feature = new Feature();
            $feature->setSlug($fData[0]);
            $feature->setName($fData[1]);
            $feature->setOpened($fData[2]);
            $this->entityManager->persist($feature);
            $this->entityManager->flush();
            $features[] = $feature;
        }
        /* End insert Features */

        /* Begin insert UserFeatures */
        foreach($features as $feature) {
            $userFeature = new UserFeature();
            $userFeature->setUser($users[1]);
            $userFeature->setFeature($feature);
            $userFeature->setEnabled(true);
            $this->entityManager->persist($userFeature);
            $this->entityManager->flush();
        }
        /* Begin insert UserFeatures */

        $this->Db_commitTransaction();

        return Command::SUCCESS;
    }

}