<?php

namespace App\Exception;

use Exception;

class TranslatableException extends Exception
{
    protected $parameters = [];
    protected $domain = 'exceptions';

    public function __construct($message = 'unexpected_happened', $parameters = [], $domain = 'exceptions', $code = 0, Exception $previous = null) {
        $this->parameters = $parameters;
        $this->domain = $domain;
        parent::__construct($message, $code, $previous);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function getDomain()
    {
        return $this->domain;
    }
}