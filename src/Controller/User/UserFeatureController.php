<?php

namespace App\Controller\User;

use App\Traits\DbTrait;
use App\Traits\ApiTrait;
use App\Traits\FormTrait;
use App\Traits\RequestTrait;
use Psr\Log\LoggerInterface;
use App\Entity\Feature\Feature;
use App\Entity\User\UserFeature;
use App\Form\User\TypeUserFeature;
use App\Form\User\GetUserFeatureType;
use App\Form\User\SetUserFeatureType;
use App\Exception\TranslatableException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api/feature")
 * @IsGranted("ROLE_ADMIN")
 */
class UserFeatureController extends AbstractController
{
    use RequestTrait, DbTrait, ApiTrait, FormTrait;

    protected $translator;

    public function __construct(
        RequestStack $requestStack, //Request stack
        EntityManagerInterface $entityManager, //DbTrait
        SerializerInterface $serializer, //ApiTrait
        TranslatorInterface $translator,
        LoggerInterface $apiLogger
    )
    {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
        $this->serializer = $serializer;
        $this->requestStack = $requestStack;
        $this->apiLogger = $apiLogger;
    }

    /**
     * @Route("", name="api_userfeature_feature", methods={"GET", "POST"})
     */
    public function feature(Request $request)
    {
        $requestMethod = $request->getMethod();
        $formType = $requestMethod === 'POST' ? SetUserFeatureType::class : GetUserFeatureType::class;
        $form = $this->createForm($formType, ($typeUserFeature = new TypeUserFeature()), [
            'entity_manager' => $this->entityManager,
            'translator' => $this->translator
        ]);

        /*
        POST
        - Parse JSON body into array
        - SetUserFeatureType::class validates email & feature, form UserFeature.
        - If UserFeature not exist, generate new UserFeature()
        - After validation success, use setEnabled() with "enable" data > persist > return 200 code.
        - If validation failed, return 304 code.
         */
        if($requestMethod === 'POST') 
        {
            $data = $this->Api_getRequestData();
            $form->submit($data);
            if ($form->isSubmitted() && $form->isValid()) 
            {
                $this->Db_beginTransaction();

                $enable = $typeUserFeature->getEnable();
                $userFeature = $typeUserFeature->getUserFeature();
                $userFeature->setEnabled($enable);
                $this->entityManager->persist($userFeature);
                $this->entityManager->flush();

                $this->Db_commitTransaction();
                return $this->Api_returnEmpty(200);
            }
            //return $this->Api_returnError($this->Form_getErrors($form));    
            return $this->Api_returnEmpty(304);
        }

        /*
        GET
        - Get email & featureName from query
        - GetUserFeatureType::class validates email & feature, form UserFeature.
        - If validation failed, return response JSON {"status" : "error", "error" : { "email" : "Err Msg", "featureName" : "ErrMsg" }}, status code 400
        - If success, return response JSON {"status" : "success", "data" : { "canAccess" : true|false }}, status code 200
         */
        $data = [
            'email' => $request->query->get('email', NULL),
            'featureName' => $request->query->get('featureName', NULL)
        ];
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) 
        {
            $canAccess = ($userFeature = $typeUserFeature->getUserFeature()) ? $userFeature->getEnabled() : false;
            return $this->Api_returnSuccess([
                'canAccess' => $canAccess
            ]);
        }
        return $this->Api_returnError($this->Form_getErrors($form));
    }

}