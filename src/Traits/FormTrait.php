<?php

namespace App\Traits;

use Symfony\Component\Form\Form;
use App\Exception\TranslatableException;

trait FormTrait
{
    public function Form_getErrors(Form $form)
    {
        $errors = [];

        foreach ($form as $fieldName => $formField) {
            foreach ($formField->getErrors(true) as $error) {
                $errors[$fieldName] = $error->getMessage();
            }
        }
        //$errorMessages = array_values($errors);
        // foreach ($form->getErrors() as $key => $error) {
        //     $errors[] = $error->getMessage();
        // }

        return $errors;
    }
}