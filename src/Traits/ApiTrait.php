<?php

namespace App\Traits;

use App\Exception\TranslatableException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

trait ApiTrait
{
    protected $serializer, $apiLogger;
    private $requestData;

    public function Api_log($statusCode, $response = [])
    {
        $context = [
            'url' => $_SERVER['REQUEST_URI'],
            'ip' => $_SERVER['REMOTE_ADDR'],
            'auth' => ($user = $this->getUser()) ? $user->getEmail() : null,
            'req' => $this->getRequestData(),
            'res' => $response
        ];
        switch($statusCode) {
            case 200:
                $this->apiLogger->info('', $context);
                break;
            default:
                $this->apiLogger->error('', $context);
                break;
        }
        
    }

    public function Api_getRequestData()
    {
        $contentType = $this->getContentType();
        if(!$contentType) { $contentType = 'application/json'; } //default json
        $content = $this->getRequest()->getContent();
        if(!$content) { $content = $this->getRequest()->request->all(); }
        if(!$content) { return []; }
        switch($contentType)
        {
            case 'application/json':
                $data = json_decode($this->getRequest()->getContent(), true);
                if ($data === null || json_last_error() != JSON_ERROR_NONE) {
                    throw new TranslatableException('invalid_content_format');
                }
                break;
            default:
                $data = $this->getRequest()->request->all();
                foreach($this->getRequest()->files as $name => $file) {
                    $data[$name] = $file;
                }
                break;
        }

        //Symfony form do not allow recieve false value, will auto converted into null, so here manually convert true/false bool into yes/no string
        foreach($data as $key => $value) {
            if(is_bool($value)) {
                $data[$key] = ($value === true ? 'yes' : 'no');
            }
        }

        $this->setRequestData($data);

        return $data;
    }

    public function Api_returnEmpty(int $statusCode)
    {
        $this->Api_log($statusCode, '', []);
        return new Response('', $statusCode);
    }

    public function Api_returnError($err, $statusCode = 400)
    {
        $response = [
            'status' => 'error',
            'error' => $err
        ];
        $this->Api_log($statusCode, '', $response);
        return JsonResponse::fromJsonString(
            $this->serializer->serialize($response, 'json'),
            $statusCode
        );
    }

    public function Api_returnSuccess($data, $statusCode = 200)
    {
        $response = [
            'status' => 'success',
            'data' => $data
        ];
        $this->Api_log($statusCode, '', $response);
        return JsonResponse::fromJsonString(
            $this->serializer->serialize($response, 'json'),
            $statusCode
        );
    }

    public function getRequestData()
    {
        return $this->requestData;
    }

    public function setRequestData($requestData)
    {
        $this->requestData = $requestData;

        return $this;
    }
}