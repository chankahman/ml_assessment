<?php

namespace App\Traits;

use Doctrine\ORM\Mapping as ORM;
use App\Exception\TranslatableException;

trait DbTrait
{
    protected $entityManager;

    public function Db_init()
    {
        if(isset($this->_em)) {
            $this->entityManager = $this->_em;
        }
        if(!$this->entityManager) {
            throw new TranslatableException('entity_manager_not_defined', ['%class%' => get_called_class()]);
        }
    }

    public function Db_beginTransaction()
    {
        $this->Db_init();
        $this->entityManager->getConnection()->beginTransaction();
    }

    public function Db_commitTransaction()
    {
        $this->Db_init();
        $this->entityManager->getConnection()->commit();
    }
    
    public function Db_rollbackTransaction()
    {
        $this->Db_init();
        $this->entityManager->getConnection()->rollback();
    }
    
    public function Db_select($sql, $params = [])
    {
        $this->Db_init();
        $types = [];
        foreach ($params as $key => $value) {
            if (gettype($value) === 'array') {
                $types[$key] = \Doctrine\DBAL\Connection::PARAM_STR_ARRAY;
            } else {
                $types[$key] = \PDO::PARAM_STR;
            }
        }
        $stmt = $this->entityManager->getConnection()->executeQuery($sql, $params, $types);
        return $stmt->fetchAll();
    }
    
    public function Db_selectFirst($sql, $params = [])
    {
        $this->Db_init();
        $result = $this->Db_select($sql, $params);
        if ($this->Db_isEmptyResult($result)) {
            return false;
        }
        return $result[0];
    }
    
    public function Db_insert($table, $params = [])
    {
        $this->Db_init();
        return $this->entityManager->getConnection()->insert($table, $params);
    }
    
    public function Db_update($sql, $params = [])
    {
        $this->Db_init();
        return $this->entityManager->getConnection()->executeUpdate($sql, $params);
    }
    
    public function Db_isEmptyResult($result)
    {
        return (is_null($result) || sizeof($result) == 0);
    }

    public function Db_useRepository($class)
    {
        $this->Db_init();
        return $this->entityManager->getRepository($class);
    }
}