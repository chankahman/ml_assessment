<?php

namespace App\Traits;

use App\Exception\TranslatableException;

trait RequestTrait
{
    protected $requestStack;

    public function getRequest()
    {
        return $this->requestStack->getCurrentRequest();
    }

    public function getLocale()
    {
        return $this->getRequest()->headers->get('Locale', $this->getParameter('default_locale'));
    }

    public function getContentType()
    {
        return $this->getRequest()->headers->get('Content-Type', NULL);
    }
}